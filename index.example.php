<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <script src="https://apis.google.com/js/platform.js" async defer></script>
  <head>
    <meta charset="utf-8">
    <meta name="google-signin-client_id" content="1023573662646-so35jf1q1i00a9sfit9aar6a3ap5k67g.apps.googleusercontent.com">
    <title>Bondour</title>
  </head>

  <body>
    <p>Le Pas-de-Calais</p>
    <div class="g-signin2" data-onsuccess="onSignIn"></div>

  </body>


  <script type="text/javascript">
    function onSignIn(googleUser) {
      var profile = googleUser.getBasicProfile();
      console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
      console.log('Name: ' + profile.getName());
      console.log('Image URL: ' + profile.getImageUrl());
      console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
    }
  </script>
</html>
